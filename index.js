/* 4 variable
      height - size off ballon 70 ~ 120
     left - 0 < left <100vw for start position 40 is center
     translate (x,y);
     x - 0 < x < |80vw|,
     if(left <40) x > 40;
     if(left >40) x <40;
     y - 60vh < y < |90vh|
     others are constant
    */

const getRandom = (start, end) => Math.floor(Math.random() * end + start);

const getPhotoNumber = () => getRandom(1, 13);

const getHeight = () => getRandom(30, 120);

const getOpacity = (height) => height / 60;

const getLeftPosition = () => getRandom(0, 80);

const getX = (leftPosition) => getRandom(0, 80) * (leftPosition < 40 ? 1 : -1);

const getY = () => -getRandom(60, 90);

function addTwoBallon() {
  addBallon();
}

function addBallon() {
  let root = document.getElementById("root");
  let ballon = createBallon();
  root.appendChild(ballon);
  let left = ballon.style.left.replace("vw", "");
  let x = getX(left);
  setTimeout(() => {
    ballon.style.opacity = 0;
    ballon.style.transform = `translate(${x}vw,${getY()}vh) rotate(${
      x / 3
    }deg)`;
    console.log("ballon", ballon.style.transform);
  }, 0);
  setTimeout(() => {
    root.removeChild(ballon);
    delete ballon;
  }, 8000); // transition is 8s, element will be deleted at 8s
}

function createBallon() {
  let ballon = document.createElement("img");
  ballon.src = `https://res-global.1315cdn.com:11443/statics/ballon/${getPhotoNumber()}.png`;
  ballon.classList.add("ballon");
  ballon.height = getHeight();
  ballon.style.zIndex = ballon.height;
  // ballon.style.opacity = getOpacity(ballon.height);
  ballon.style.left = getLeftPosition() + "vw";
  return ballon;
}
